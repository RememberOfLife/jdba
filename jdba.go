package jdba

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
	"sync"
	"time"
)

// DC is the data connection that provides interaction between files cache and requests
type DC struct {
	Minify    bool              // if written files should be minified or not
	timeout   uint64            // duration in seconds after which a cache item, if not accessed, will be written back to its key file path
	accessMap map[string]uint64 // records the last access time for every cache item
	modifyMap map[string]bool   // labels wether an item has been modified or not and should be written back
	dataCache map[string][]byte // cache of the data
	close     chan struct{}
	// LOCK ORDER:
	dataPathMapLock *sync.Mutex              // restricts access to the dataPathLocks, accessMap, modifyMap
	dataPathLocks   map[string]*sync.RWMutex // restricts access to the dataCache
}

// NewJDBA returns a new data connection with the given configuration, timeout is the lifetime in seconds
func NewJDBA(timeout uint64, minify bool) *DC {
	if timeout < 1 {
		return nil
	}
	dc := &DC{
		Minify:          minify,
		timeout:         timeout,
		accessMap:       make(map[string]uint64),
		modifyMap:       make(map[string]bool),
		dataCache:       make(map[string][]byte),
		close:           make(chan struct{}),
		dataPathMapLock: new(sync.Mutex),
		dataPathLocks:   make(map[string]*sync.RWMutex),
	}
	// start cleanup routine for the dc
	go func() {
		ticker := time.NewTicker(time.Duration(timeout) * time.Second)
		defer ticker.Stop()
		for {
			select {
			case t := <-ticker.C:
				dc.CleanUp(uint64(t.Unix()))
			case <-dc.close:
				return
			}
		}
	}()
	return dc
}

// Reset clears the cache without write-back, data will be lost
func (dc *DC) Reset() {
	dc.dataPathMapLock.Lock()
	defer dc.dataPathMapLock.Unlock()
	for _, dpl := range dc.dataPathLocks {
		dpl.Lock()
	}
	dc.accessMap = make(map[string]uint64)
	dc.modifyMap = make(map[string]bool)
	dc.dataCache = make(map[string][]byte)
	dc.dataPathLocks = make(map[string]*sync.RWMutex)
}

// WriteBackAll forces a write-back of all cache entries and clears the cache
func (dc *DC) WriteBackAll() {
	dc.dataPathMapLock.Lock()
	defer dc.dataPathMapLock.Unlock()
	for dp := range dc.dataCache {
		// write-back and drop
		dc.cacheWriteBack(dp)
		dc.cacheDropEntry(dp)
	}
}

// CleanUp initiates cache eviction and write back
func (dc *DC) CleanUp(callTime uint64) {
	dc.dataPathMapLock.Lock()
	defer dc.dataPathMapLock.Unlock()
	dropList := []string{}
	for dp, accessTime := range dc.accessMap {
		if callTime >= accessTime+dc.timeout {
			dropList = append(dropList, dp)
		}
	}
	// drop everything in the drop list
	for _, dp := range dropList {
		// write-back and drop
		dc.cacheWriteBack(dp)
		dc.cacheDropEntry(dp)
	}
}

// Close will stop the cleanup routine for the dc so it may be safely discarded
func (dc *DC) Close() {
	close(dc.close)
}

// only use with possension of the dataPathMapLock
func (dc *DC) cacheWriteBack(p string) error {
	dc.cacheCreateDataPath(p)
	dc.dataPathLocks[p].Lock()
	defer dc.dataPathLocks[p].Unlock()
	if !dc.modifyMap[p] {
		return nil
	}
	return saveFile(p, dc.dataCache[p])
}

// only use with possension of the dataPathMapLock
func (dc *DC) cacheCreateDataPath(p string) {
	if _, ok := dc.dataPathLocks[p]; !ok {
		dc.dataPathLocks[p] = new(sync.RWMutex)
	}
}

// only use with possension of the dataPathMapLock
func (dc *DC) cacheUpdateAccess(p string, modifying bool) {
	if _, ok := dc.modifyMap[p]; !ok {
		dc.modifyMap[p] = modifying
	} else {
		dc.modifyMap[p] = dc.modifyMap[p] || modifying
	}
	dc.accessMap[p] = uint64(time.Now().Unix())
}

// only use with possension of the dataPathMapLock
func (dc *DC) cacheDropEntry(p string) {
	dc.cacheCreateDataPath(p)
	dc.dataPathLocks[p].Lock()
	delete(dc.dataCache, p)
	delete(dc.dataPathLocks, p)
	delete(dc.accessMap, p)
	delete(dc.modifyMap, p)
}

// Drop removes the cache entry for the target file
func (dc *DC) Drop(p string) {
	dc.dataPathMapLock.Lock()
	defer dc.dataPathMapLock.Unlock()
	dc.cacheDropEntry(p)
}

// LoadSingle functions like Load but the cache entry will be dropped as fast as possible instead of waiting for a timeout
func (dc *DC) LoadSingle(p string, tS interface{}) error {
	defer dc.Drop(p)
	return dc.Load(p, tS)
}

// Load will fill the provided json object with data from the file at the given path
func (dc *DC) Load(p string, tS interface{}) error {
	dc.dataPathMapLock.Lock()
	dc.cacheCreateDataPath(p)
	dc.dataPathLocks[p].RLock()
	defer dc.dataPathLocks[p].RUnlock()
	dc.cacheUpdateAccess(p, false)
	dc.dataPathMapLock.Unlock()
	// now in possession of only entry specific lock
	if data, ok := dc.dataCache[p]; ok {
		return jsonDeSerialize(data, tS)
	}
	// cache miss
	// check if file exists
	if _, err := os.Stat(p); os.IsNotExist(err) {
		return err
	}
	data, err := loadFile(p)
	if err != nil {
		return err
	}
	dc.dataCache[p] = data
	return jsonDeSerialize(data, tS)

}

// WriteBack will perform write-back on a single entry of the cache and remove it, returns true if successful
func (dc *DC) WriteBack(p string) bool {
	dc.dataPathMapLock.Lock()
	defer dc.dataPathMapLock.Unlock()
	dc.dataPathLocks[p].Lock()
	if _, ok := dc.dataCache[p]; !ok {
		return false
	}
	dc.dataPathLocks[p].Unlock()
	dc.cacheWriteBack(p)
	dc.cacheDropEntry(p)
	return true
}

// SaveSingle functions like Save but the cache entry will perform write-back as fast as possible instead of waiting for a timeout
func (dc *DC) SaveSingle(p string, tS interface{}) {
	defer dc.WriteBack(p)
	dc.Save(p, tS)
}

// Save to the given path the provided json object
func (dc *DC) Save(p string, tS interface{}) error {
	dc.dataPathMapLock.Lock()
	dc.cacheCreateDataPath(p)
	dc.dataPathLocks[p].Lock()
	defer dc.dataPathLocks[p].Unlock()
	dc.cacheUpdateAccess(p, true)
	dc.dataPathMapLock.Unlock()
	var err error
	dc.dataCache[p], err = jsonSerialize(tS, dc.Minify)
	if err != nil { //TODO potentially already skip here if item already exists, can save file ex. check
		return err
	}
	// check if file exists, if not this is creation so instant writeback is at order to keep fs name consistency
	if _, ferr := os.Stat(p); os.IsNotExist(ferr) {
		// first time writeback
		err = saveFile(p, dc.dataCache[p])
	}
	return err
}

// Delete drops the file from cache and deletes it from the FS
func (dc *DC) Delete(p string) error {
	dc.dataPathMapLock.Lock()
	defer dc.dataPathMapLock.Unlock()
	dc.cacheDropEntry(p)
	return delFile(p)
}

func jsonSerialize(tS interface{}, minify bool) ([]byte, error) {
	if minify {
		return json.Marshal(tS)
	}
	return json.MarshalIndent(tS, "", "\t")
}

func jsonDeSerialize(data []byte, tS interface{}) error {
	return json.Unmarshal(data, tS)
}

func loadFile(p string) ([]byte, error) {
	return ioutil.ReadFile(p)
}

func saveFile(p string, data []byte) error {
	// create parent path if necessary
	if err := os.MkdirAll(filepath.Dir(p), 0700); err != nil {
		return err
	}
	err := ioutil.WriteFile(p, data, 0700)
	return err
}

func delFile(p string) error {
	// remove target file
	return os.Remove(p)
}

// GetAccessMap returns
func (dc *DC) GetAccessMap() map[string]uint64 {
	dc.dataPathMapLock.Lock()
	defer dc.dataPathMapLock.Unlock()
	retMap := make(map[string]uint64)
	// copy the map to the return
	for k, v := range dc.accessMap {
		if dc.modifyMap[k] {
			retMap[k] = v
		} else {
			retMap[k] = 0
		}
	}
	return retMap
}
