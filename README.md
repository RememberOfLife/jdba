# jdba

A concurrency friendly flat-file json helper with integrated caching, written in GO. (Mainly intended for small projects.)

Note: *there is currently no support for custom json deserializers.*